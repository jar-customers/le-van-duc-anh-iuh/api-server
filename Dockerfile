
# INSTALL LIBS ============================

FROM f26raptor/yolov7

WORKDIR /usr/my_app/app

# INSTALL LIBS ============================

RUN pip3 install Flask-Cors
RUN pip3 install keras
RUN pip3 install tensorflow
RUN pip3 install pymongo


# ENV ==================================



# ======================================

COPY ./src ./src
COPY ./data ./data

# ====

EXPOSE 3000

CMD [ "python3", "./src/main.py" ]
