from typing import Union
import time
from pathlib import Path
import os
import numpy as np
import torch
import cv2
import json

from entities.size import Size
from entities.yolo_object import YoloObject
from models.experimental import attempt_load
from utils.general import non_max_suppression
from funcs import local_path
from funcs import xcalc

class YoloV7Detector():
    def __init__(self, absolute_weight_path:str, default_threshold=0.5, id:Union[str, None] = None) -> None:
        # default
        self._default_threshold = 0.5
        self._model = None
        self._weight_path = None
        self._config_path = None
        self._device = torch.device('cpu')
        self._stride = 32
        self._img_size = 640

        _conf_threshold = default_threshold or 0.5
        self._names:list[str] = []
        self._id = id
        
        self.set_threshold(_conf_threshold)
        self.set_weight(absolute_weight_path)

    
    def set_threshold(self, v:float):
        if type(v) is not float:
            print(f'warning: threshold must be float. Current threshold: {self.conf_threshold}')
            return

        if v < 0 or v > 1:
            print(f'warning: threshold must be greater than or equal to 0 and lesser than or equal to 1. Current threshold: {self.set_threshold}')
            return
        
        self.conf_threshold = v


    def get_threshold(self):
        return self.conf_threshold



    def set_weight(self, absolute_weight_dir:Path):
        print('[INFO] Loading model')

        weight_name = os.path.basename(absolute_weight_dir)
        weight_path = local_path.realpath(absolute_weight_dir, weight_name + '.pt')
        config_path = local_path.realpath(absolute_weight_dir, weight_name + '.config.json')

        if not os.path.exists(weight_path):
            print(f'[ERROR] Path of weight is not exist: "{weight_path}"')
            return False

        if not os.path.exists(config_path):
            print(f'[ERROR] Path of config is not exist: "{config_path}"')
            return False

        self._weight_path = weight_path
        self._config_path = config_path

        # Load model
        self._model = attempt_load(weight_path, map_location=self._device)
        self._stride = int(self._model.stride.max())
        self._names = list(self._model.names)
        print('[SUCCESS] Load detector model success')

        # Load config
        with open(config_path, 'r', encoding='utf-8') as f:
            str_json = f.read()
        
        config = json.loads(str_json)
        self._img_size = int(config['imageSize'])
        print('[SUCCESS] Load detector model config success')

        if self._id == None:
            self._id = weight_name
        


    def get_names(self) -> list:
        return self._names


    
    def _normalize_prediction(self, original_img_shape:list, predicted_image_size:list, pred, time_to_predict:float, threshold:float, original_img:np.ndarray):
        predicted_width, predicted_height = [float(v) for v in predicted_image_size]
        original_height, original_width, _ = original_img_shape

        yolo_object = YoloObject(
            original_size=Size(width=original_width, height=original_height),
            predicted_size=Size(width=predicted_width, height=predicted_height)
        )

        yolo_object.load_items(pred[0], self._names)

        return yolo_object



    def detect(self, img0:np.ndarray, threshold:Union[float, None]=None, iou_thres=0.45):
        conf_thres = self.conf_threshold

        if type(threshold) is float and (0 < threshold < 1):
            conf_thres = threshold
        
        t_start = time.time()

        img = self._prev_process_image(img0)
        pred = self._model(img, augment=True)[0]
        pred = non_max_suppression(pred, conf_thres, iou_thres, classes=None, agnostic=True)

        img_size = list(img.size())[2:]
        img_size.reverse()


        return self._normalize_prediction(
            pred=pred,
            original_img_shape=img0.shape,
            threshold=conf_thres,
            time_to_predict=float(time.time() - t_start),
            predicted_image_size=img_size,
            original_img=img0
        )


    def detect_all_objects(self, img0:np.ndarray, threshold:Union[float, None]=None, iou_thres=0.45, padding=0.1, output_size:int=256):
        pred = self.detect(img0, threshold, iou_thres)
        if len(pred.items) == 0:
            return []

        result = []
        height, width = img0.shape[0:2]
        float_height, float_width = float(height), float(width)

        for item in pred.items:
            top = xcalc.real_value(item.top, float_height)
            left = xcalc.real_value(item.left, float_width)
            bottom = xcalc.real_value(item.top + item.height, float_height)
            right = xcalc.real_value(item.left + item.width, float_width)
            object_width = xcalc.real_value(item.width, float_width)
            object_height = xcalc.real_value(item.height, float_height)
            half_padding_horizon = object_width*padding/2
            half_padding_vertical = object_height*padding/2

            final_top = xcalc.clamp_round_int(top - half_padding_vertical, float_height)
            final_left = xcalc.clamp_round_int(left - half_padding_horizon, float_width)
            final_bottom = xcalc.clamp_round_int(bottom + half_padding_vertical, float_height)
            final_right = xcalc.clamp_round_int(right + half_padding_horizon, float_width)

            if object_height > object_width:
                center = (left + right)/2
                delta = (final_bottom - final_top)/2
                final_left = xcalc.clamp_round_int(center - delta, float_width)
                final_right = xcalc.clamp_round_int(center + delta, float_width)

            elif object_width > object_height:
                center = (top + bottom)/2
                delta = (final_right - final_left)/2
                final_top = xcalc.clamp_round_int(center - delta, float_height)
                final_bottom = xcalc.clamp_round_int(center + delta, float_height)

            object_img = img0[final_top:final_bottom, final_left:final_right]
            final_object_width, final_object_height = object_img.shape[0:2]
            final_aspect_ratio = float(final_object_width)/float(final_object_height)

            dsize = (output_size, output_size)
            if final_object_width > final_object_height:
                dsize = (
                    output_size,
                    min(int(output_size/final_aspect_ratio), output_size)
                )
            elif final_object_width < final_object_height:
                dsize = (
                    min(int(output_size*final_aspect_ratio), output_size),
                    output_size
                )
            
            object_img = cv2.resize(object_img, dsize)
            tmp_h, tmp_w = object_img.shape[0:2]

            final_object_img:np.ndarray = None
            if tmp_h != tmp_w:
                final_object_img = np.zeros((output_size, output_size, 3), dtype=object_img.dtype)
                final_object_img[:tmp_h, :tmp_w] = object_img[:tmp_h, :tmp_w]
            else:
                final_object_img = object_img
            

            result.append([item.labelname, final_object_img, item])

        return result, pred

        

    
    def _prev_process_image(self, img0:np.ndarray):
        # Padded resize
        img = self._letterbox(img0)[0]

        # Convert
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)

        #
        img = torch.from_numpy(img).to(self._device)
        img = img.half()
        img = img.float()
        img /= 255.0  # 0 - 255 to 0.0 - 1.0

        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        return img


    def _letterbox(self, img, color=(114, 114, 114), auto=True, scaleFill=False, scaleup=True):
        new_shape = self._img_size

        # Resize and pad image while meeting stride-multiple constraints
        shape = img.shape[:2]  # current shape [height, width]
        if isinstance(new_shape, int):
            new_shape = (new_shape, new_shape)

        # Scale ratio (new / old)
        r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
        if not scaleup:  # only scale down, do not scale up (for better test mAP)
            r = min(r, 1.0)

        # Compute padding
        ratio = r, r  # width, height ratios
        new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
        dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
        if auto:  # minimum rectangle
            dw, dh = np.mod(dw, self._stride), np.mod(dh, self._stride)  # wh padding
        elif scaleFill:  # stretch
            dw, dh = 0.0, 0.0
            new_unpad = (new_shape[1], new_shape[0])
            ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]  # width, height ratios

        dw /= 2  # divide padding into 2 sides
        dh /= 2

        if shape[::-1] != new_unpad:  # resize
            img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
        top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
        left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
        img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
        return img, ratio, (dw, dh)

