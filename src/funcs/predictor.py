import os
import numpy as np
import cv2
import json

from keras.models import load_model
from funcs import local_path

class Predictor():
    def __init__(self, absolute_weight_dir:str) -> None:
        self._labels:list[str] = []
        self._image_size:int = 256
        self.set_weight(absolute_weight_dir)


    @property
    def image_size(self):
        return self._image_size

    # Tiền xử lý hình ảnh trước khi đưa vào dự đoán
    def _pre_processing_image(self, img:np.ndarray):
        img_result = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) # Chuyển đổi hệ số màu BGR => RGB
        img_result = cv2.resize(img, dsize=(self._image_size, self._image_size)) # Resize image sao cho bàng với input shape
        img_result = img_result/255 # [0->255] => [0->1]
        return img_result

    def set_weight(self, absolute_weight_dir:str):
        weight_name = os.path.basename(absolute_weight_dir)
        weight_path = local_path.realpath(absolute_weight_dir, weight_name + '.h5')
        config_path = local_path.realpath(absolute_weight_dir, weight_name + '.json')
        self._model = load_model(weight_path)
        config = {}

        with open(config_path, 'r', encoding='utf-8') as f:
            config = json.loads(f.read())
        
        self._image_size = int(config['imageSize'])
        self._labels = config['labels']

        print('[SUCCESS] Load predictor model success')


    def predict(self, original_img:np.ndarray):
        img = self._pre_processing_image(original_img)
        x = np.array([img])
        pred = np.argmax(self._model.predict(x)[0])
        return pred, self._labels[pred]

        