import numpy as np
import cv2
import base64

def from_base64(encoded_data:str)->np.ndarray:
    np_arr = np.frombuffer(base64.b64decode(encoded_data), np.uint8)
    img = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    return img