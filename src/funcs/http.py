import json
from typing import Union, Literal
from collections.abc import Callable
from flask import request

class Req():
    def __init__(self, body:str, headers:dict, method:str, query:dict) -> None:
        self._headers = headers
        self._error_message = ''
        self._is_ok:bool = True
        self._body: Union[str, dict, None] = None
        self._method: Literal['GET','POST','OPTION','HEAD','PUT'] = method
        self._query:dict = query

        try:
            if 'Content-Type' in headers:
                # process Content-Type
                content_type_raw:str = headers['Content-Type']
                content_type_splitted = content_type_raw.split(';')
                content_type:list[str] = []
                for item in content_type_splitted:
                    content_type.append(item.strip())

                if 'application/json' in content_type:
                    self._body = json.loads(body)
                else:
                    self._body = body
        except:
            print(f'Request with wrong body')
            self._error_message = 'Parse to json failed !'
            self._is_ok = False

    
    def get_body(self):
        return self._body

    def get_header(self):
        return self._headers

    def get_method(self):
        return self._method

    def get_query(self):
        return self._query

    def is_ok(self):
        return self._is_ok




class Res():
    def __init__(self) -> None:
        self._body:Union[dict, str] = ''
        self._headers:dict = {}
        self._status:int = 200
        self._is_end:bool = False

    def set_body(self, body:Union[dict, str]):
        self._body = body
        return self

    def get_body(self):
        return self._body


    def set_headers(self, headers:Union[dict, list[str]]):
        if (type(headers) is list) and (len(headers) == 2):
            self._headers[headers[0]] = headers[1]

        if (type(headers) is dict):
            self._headers = headers

        return self


    def get_headers(self):
        return self._headers


    def set_status(self, status:int):
        if (type(status) is int) and (100 <= status <= 599):
            self._status = status
        else:
            self._status = 200
        return self


    def get_status(self):
        return self._status



    def end(self):
        self._is_end = True
        return self




# routers: nhận vào một list các callback
def run_all(routers:list[Callable[[Req, Res], bool]]):
    if len(routers) == 0:
        return '', 202

    req = Req(
        body=request.data,
        headers=request.headers,
        method=request.method,
        query=request.args,
    )

    if not req.is_ok():
        return req._error_message, 400

    res = Res()

    for router in routers:

        # if router(req, res) == False:
        #     return '', 202

        try:
            if router(req, res) == False:
                return '', 202
        except:
            return 'Server ERROR !', 500
        
        if res._is_end:
            return res._body, res._status, res._headers
    

    return res._body, res._status, res._headers