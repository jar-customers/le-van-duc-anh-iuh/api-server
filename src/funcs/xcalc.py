def clamp(min:float, value:float, max:float):
    if value > max:
        return max

    if value < min:
        return min

    return value


def round_int(v:float):
    return int(round(v))


def clamp_round_int(v:float, max_value:float, min_value:int=0):
    return int(round(clamp(min_value, v, max_value)))

def real_value(percent:float, max_value:float):
    return clamp(0.0, percent*max_value, max_value)