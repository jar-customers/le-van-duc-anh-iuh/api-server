from typing import Union
import numpy as np
import json
from entities.size import Size

class YoloObjectItem():
    def __init__(self,
        top:float = 0.0,
        left:float = 0.0,
        width:float = 0.0,
        height:float = 0.0,
        confidence:float = 0,
        label:Union[int, None] = None,
        labelname:Union[str, None] = None
    ) -> None:
        # Init
        self._top:float = 0.0
        self._left:float = 0.0
        self._width:float = 0.0
        self._height:float = 0.0
        self._confidence:float = 0.0
        self._label:Union[int, None] = label
        self._labelname:Union[str, None] = labelname

        # Set
        self.top:float = top
        self.left:float = left
        self.width:float = width
        self.height:float = height


    @property
    def top(self):
        return self._top

    @top.setter
    def top(self, value:float):
        if (not type(value) is float) or (value < 0 or value > 1):
            self._left = 0
            print('[ERROR, YoloObjectItem] Value of top must greater than 0 and less than 1')
            return
        self._top = value


    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, value:float):
        if (not type(value) is float) or (value < 0 or value > 1):
            self._left = 0
            print('[ERROR, YoloObjectItem] Value of left must greater than 0 and less than 1')
            return
        self._left = value


    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value:float):
        if (not type(value) is float) or (value < 0 or value > 1):
            self._left = 0
            print('[ERROR, YoloObjectItem] Value of width must greater than 0 and less than 1')
            return
        self._width = value


    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value:float):
        if (not type(value) is float) or (value < 0 or value > 1):
            self._left = 0
            print('[ERROR, YoloObjectItem] Value of height must greater than 0 and less than 1')
            return
        self._height = value


    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value:Union[int, None]):
        self._label = value


    @property
    def labelname(self):
        return self._labelname

    @labelname.setter
    def labelname(self, value:Union[str, None]):
        self._labelname = value


    @property
    def confidence(self):
        return self._confidence

    @confidence.setter
    def confidence(self, value:float):
        self._confidence = value

    def load(self, pred_item, predicted_size:Size, labelnames:list[str]):
        left, top, right, bottom, confidence, label = pred_item.detach().numpy()

        self.top = float(top)/predicted_size.height
        self.left = float(left)/predicted_size.width
        self.width = float(right - left)/predicted_size.width
        self.height = float(bottom - top)/predicted_size.height

        digi_label = int(label)
        self.label = digi_label
        self.labelname = str(labelnames[digi_label])
        self.confidence = float(confidence)


    def to_dict(self)->dict:
        return {
            'top': self._top,
            'left': self._left,
            'width': self._width,
            'height': self._height,
            'confidence': self._confidence,
            'label': self._label,
            'labelname': self._labelname
        }

    def __str__(self) -> str:
        pass





class YoloObject():
    def __init__(self,
        original_size:Size,
        predicted_size:Size,
    ) -> None:
        self._original_size:Size = original_size
        self._predicted_size:Size = predicted_size
        self._items:list[YoloObjectItem] = []


    @property
    def original_size(self):
        return self._original_size

    @property
    def predicted_size(self):
        return self._predicted_size

    @property
    def items(self):
        return self._items



    def load_items(self, pred, labelnames:list[str]):
        items:list[YoloObjectItem] = []

        for element in pred:
            item = YoloObjectItem()
            item.load(
                pred_item=element,
                predicted_size=self._predicted_size,
                labelnames=labelnames
            )
            items.append(item)

        self._items = items


    def to_dict(self)->dict:
        items:list = []
        for element in self._items:
            items.append(element.to_dict())
        
        return {
            'originalSize': self._original_size.to_dict(),
            'predictedSize': self._predicted_size.to_dict(),
            'items': items
        }

    def __str__(self) -> str:
        return json.dumps(self.to_dict())






