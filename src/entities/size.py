class Size():
    def __init__(self, width:float=0.0, height:float=0.0) -> None:
        self._width = width
        self._height = height

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, v):
        self._width = v

    @property
    def height(self):
        return self._height


    @height.setter
    def height(self, v):
        self._height = v

    def to_dict(self)->dict:
        return{
            'width': self.width,
            'height': self.height
        }