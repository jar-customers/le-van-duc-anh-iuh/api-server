import pymongo

from config import local_env


MY_CLIENT = pymongo.MongoClient(local_env.CONNECTION_STRING)



MY_DB = MY_CLIENT[local_env.DB_NAME]


HISTORIES_COL = MY_DB['histories']

try:
    print('[INFO] Trying to connect to database...')
    MY_CLIENT.server_info()
    print('[INFO] Connect to database success')
except NameError:
    raise('[ERROR] Connect to database failed')
