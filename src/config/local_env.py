import os

PORT:int = int(os.environ.get('PORT') or 3000)
DB_NAME:str = os.environ.get('DB_NAME') or 'diemDanhBangKhuonMat'
CONNECTION_STRING:str = os.environ.get('CONNECTION_STRING') or 'mongodb://host.docker.internal:27017/'
ROOT_DIR:str = os.path.realpath(os.path.join(os.path.dirname(__file__), '../../'))
CW_DIR:str = os.getcwd()
FLASK_ENV:str = os.environ.get('FLASK_ENV') or 'production'

os.environ['FLASK_DEBUG'] = FLASK_ENV


VOLUME_DIR:str = None
PUBLIC_DIR:str = None
PUBLIC_IMAGES_DIR:str = None
PUBLIC_JSONS_DIR:str = None


print(f'{FLASK_ENV=}')
print(f'{CONNECTION_STRING=}')
print(f'{DB_NAME=}')
print(f'{PORT=}')
print(f'{ROOT_DIR=}')
