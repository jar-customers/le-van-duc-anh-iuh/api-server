from config import local_env
from funcs.yolov7_detector import YoloV7Detector
from funcs import local_path
from funcs.predictor import Predictor


face_detector = YoloV7Detector(
    absolute_weight_path=local_path.from_current_file(__file__, '../../data/weights/face/')
)



# Đọc cái này
face_predictor = Predictor(
    absolute_weight_dir=local_path.realpath(local_env.VOLUME_DIR, "./weights/diem-danh-khuon-mat")
)