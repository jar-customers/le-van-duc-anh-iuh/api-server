import sys
import os
from config import local_env
from funcs import local_path

# Turn off GPU
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

# Stop tensorflow log
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

# ===========================

local_env.VOLUME_DIR = local_path.from_cd_dir('./data/v')
if not local_path.is_exists(local_env.VOLUME_DIR):
    sys.exit('VOLUME_DIR is required !')


local_env.PUBLIC_DIR = local_path.realpath(local_env.VOLUME_DIR, 'public')
local_env.PUBLIC_IMAGES_DIR = local_path.realpath(local_env.PUBLIC_DIR, 'images')
local_env.PUBLIC_JSONS_DIR = local_path.realpath(local_env.PUBLIC_DIR, 'jsons')


local_path.make_sure_dir_exists(local_env.PUBLIC_IMAGES_DIR)
local_path.make_sure_dir_exists(local_env.PUBLIC_JSONS_DIR)

print(f'{local_env.VOLUME_DIR=}')
print('[SUCCESS] Init success !')