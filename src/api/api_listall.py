from funcs.http import Req, Res
from db import db

def list_all(req:Req, res:Res):
    results = []
    for item in db.HISTORIES_COL.find():
        results.append(item)

    return res.set_status(200).set_body({
        'data': results
    }).end()


def find_one(req:Req, res:Res):
    _id = req.get_query().get('id')

    result = db.HISTORIES_COL.find_one({'_id': _id})
    if not result:
        return res.set_status(404).set_body('').end()

    return res.set_status(200).set_body(result).end()