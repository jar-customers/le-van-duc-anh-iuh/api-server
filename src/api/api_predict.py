from funcs.http import Req, Res
from funcs import image_reader
from entities.yolo_object import YoloObjectItem
from uuid import uuid4
import json

from config.instance import face_detector, face_predictor
from config import local_env
from funcs import local_path
from PIL import Image
import cv2
from db import db
import datetime

def get_current_time():
    created_at = str(datetime.datetime.utcnow().isoformat())
    if created_at[-1] != 'Z':
        if len(created_at) != 23:
            created_at = created_at[0:23]
        created_at = created_at + 'Z'
    return created_at


def predict(req:Req, res:Res):
    body = req.get_body()
    img = image_reader.from_base64(body['file']['data'])

    faces, detect_pred = face_detector.detect_all_objects(img, padding=0.2, output_size=face_predictor.image_size)

    result = {
        '_id': str(uuid4()),
        'originalSize': detect_pred.original_size.to_dict(),
        "items": [],
        "createdAt": get_current_time()
    }
    
    
    for label, face, detect_pred_item in faces:
        if label != 'face':
            continue

        detect_item:YoloObjectItem = detect_pred_item
        face_pred, face_name_pred = face_predictor.predict(face)
        
        result['items'].append({
            '_id': str(uuid4()),
            'top': detect_item.top,
            'left': detect_item.left,
            'width': detect_item.width,
            'height': detect_item.height,
            'labels': [
                {
                    'digital': int(face_pred),
                    'name': str(face_name_pred)
                }
            ]
        })
    
    with open(local_path.realpath(local_env.PUBLIC_JSONS_DIR, result['_id'] + '.json'),  'w', encoding='utf-8') as f:
        f.write(json.dumps(result))
        f.close()

    
    print(result)
    # SAVE to DB
    db.HISTORIES_COL.insert_one(result)


    pil_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    pil_img = Image.fromarray(pil_img)
    pil_img.save(
        local_path.realpath(local_env.PUBLIC_IMAGES_DIR, result['_id'] + '.webp'),
        format='webp',
        lossless=True,
        quality=100,
        method=6
    )

    res.set_status(201).set_body(result).end()
    return
        
        