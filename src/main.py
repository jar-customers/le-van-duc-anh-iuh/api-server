import os

import warnings
warnings.filterwarnings("ignore")

from config import local_env
import config.init_all

from db import db

from flask import Flask
from flask_cors import CORS, cross_origin

import config.instance
from funcs.http import run_all # Bỏ qua, không đọc
from middleware import cors  # Bỏ qua, không đọc
from api import api_predict, api_listall



app = Flask(__name__, static_folder=local_env.PUBLIC_DIR, static_url_path='/static')
CORS(app)

@app.route('/', methods=['GET'])
def index():
    return (os.environ.get('APP_NAME') or 'Tên của ứng dụng')


@app.route('/predict', methods=['POST'])
def predict():
    return run_all([
        cors.access_control,
        api_predict.predict
    ])

@app.route('/list-all', methods=['GET'])
def list_all():
    return run_all([
        cors.access_control,
        api_listall.list_all
    ])

@app.route('/find-one', methods=['GET'])
def find_one():
    return run_all([
        cors.access_control,
        api_listall.find_one
    ])


def main():
    print('[INFO] App running...')
    if local_env.FLASK_ENV == 'development':
        app.run('0.0.0.0', port=local_env.PORT, debug=True)
    else:
        from waitress import serve
        serve(app, host="0.0.0.0", port=local_env.PORT)

if __name__ == '__main__':
    main()