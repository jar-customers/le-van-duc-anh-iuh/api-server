from funcs.http import Req, Res
def access_control(req:Req, res:Res):
    res.set_headers({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials':'true',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, auth, authorization',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        # 'Access-Control-Expose-Headers': 'Content-Type',
    })

    if req.get_method().upper() == 'OPTION':
        res.set_status(202).end()
        return
    
    
    